import { render, screen, fireEvent } from "@testing-library/react";
import App from "../src/App";

describe("UI test 2", () => {
  it("write text in the input", () => {
    
    render(<App />);

    
    const input = screen.getByRole('textbox');

    
    fireEvent.change(input, { target: { value: 'test test test' } });

    
    expect(input.value).toBe('test test test');
  });
});