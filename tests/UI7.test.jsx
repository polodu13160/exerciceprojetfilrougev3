import { render, fireEvent, screen } from '@testing-library/react';
import App from "../src/App";

test('As a user I want to receive "Whoa, calmos !" when I type all words upcase', () => {
  render(<App />);

  const input = screen.getByRole('textbox');
  fireEvent.change(input, { target: { value: 'TU ES STUPIDE' } });

  const button = screen.getByRole('button');
  fireEvent.click(button);

  expect(screen.getByText('Whoa, calmos !')).toBeInTheDocument();
});

