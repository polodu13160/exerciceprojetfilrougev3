import { render, screen } from "@testing-library/react";
import App from "../src/App";

describe("UI test 4", () => {
  it("should display the teenager's thought bubble with the text 'GGGggggggghhhh !'", () => {
   
    render(<App />);

    
    expect(screen.getByText("GGGggggggghhhh !")).toBeInTheDocument();
  });
});