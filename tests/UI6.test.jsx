import { render, fireEvent, screen } from '@testing-library/react';
import App from "../src/App";

test('should display "Bien sur" when "salut ?" is entered and Speak is clicked', () => {
  render(<App />);

  const input = screen.getByRole('textbox');
  fireEvent.change(input, { target: { value: 'salut ?' } });

  const button = screen.getByRole('button');
  fireEvent.click(button);

  expect(screen.getByText('Bien sûr')).toBeInTheDocument();
});


// Pq ca ne marchait pas ?? 
// Meme un console log sur le input.value ne renvoyait rien

// import { render, screen } from '@testing-library/react';
// import userEvent from '@testing-library/user-event';
// import App from "../src/App";

// test('should display "Bien sur" when "salut ?" is entered and Speak is clicked', () => {
//   render(<App />);

//   const input = screen.getByRole('textbox');
//   userEvent.type(input, 'salut ?');

//   const button = screen.getByRole('button');
//   userEvent.click(button);

//   expect(screen.getByText('Bien sûr')).toBeInTheDocument();
// });