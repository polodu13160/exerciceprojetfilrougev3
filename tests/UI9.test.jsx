import { render, fireEvent, screen } from '@testing-library/react';
import App from "../src/App";

test("As a user I want to receive Bien, on fait comme ça ! when I type nothing"
, () => {
  render(<App />);

  const input = screen.getByRole('textbox');
  fireEvent.change(input, { target: { value: '     ' } });

  const button = screen.getByRole('button');
  fireEvent.click(button);

  expect(screen.getByText('Bien, on fait comme ça !')).toBeInTheDocument();
});

