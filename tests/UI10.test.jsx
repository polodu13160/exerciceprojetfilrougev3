import { render, fireEvent, screen } from '@testing-library/react';
import App from "../src/App";

test('should display "Bien sur" when "salut ?" is entered and Speak is clicked', () => {
  render(<App />);

  const input = screen.getByRole('textbox');
  fireEvent.change(input, { target: { value: 'scdzjkchezdfc fhj H Ij ?' } });

  const button = screen.getByRole('button');
  fireEvent.click(button);

  expect(screen.getByText('Peu importe !')).toBeInTheDocument();
});

