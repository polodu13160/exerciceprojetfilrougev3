import React, { useState } from "react";
import image from "./img/001.jpg";

function App() {
  const [teenagerMessage, setTeenagerMessage] = useState("GGGggggggghhhh !");
  const [userInput, setUserInput] = useState(""); 

  const handleSubmit = () => {
  const isEmptyOrSpaces = /^\s*$/.test(userInput);
  const isLowercaseQuestion = /^[a-zà-ÿa-zà-ÿ\s\-\']+\?$/.test(userInput);
  const isUppercase = /^[A-ZÀ\s\-\',!]+$/.test(userInput);
  const isUppercaseQuestion = /^[A-ZÀ\s\-\']+\?$/.test(userInput);
   
  if (isEmptyOrSpaces) {
    setTeenagerMessage("Bien, on fait comme ça !");
  } else if (isLowercaseQuestion) {
    setTeenagerMessage("Bien sûr");
  } else if (isUppercaseQuestion) {
    setTeenagerMessage("Calmez-vous, je sais ce que je fais !");
  } else if (isUppercase) {
    setTeenagerMessage("Whoa, calmos !");
  } else {
    setTeenagerMessage("Peu importe !");
  }
};

  const handleInputChange = (event) => { 
    setUserInput(event.target.value);
  };

  return (
    <div className="container">
      <div className="row my-5">
        <div className="col">
          <h1 className="text-center">Teenage Boy Speaking</h1>
        </div>
      </div>
      <div className="row my-3"> 
        <div className="col-4"> 
          <img src={image} alt="ado" className="img-fluid" /> 
        </div>
        <div className="col-8 pt-5"> 
          <div className="mt-5 thought">
            {teenagerMessage}
          </div>
        </div>
      </div>
      <div className="row "> 
        <div className="col-12">
          <hr /> 
        </div>
        <div className="col text-center">
          <form action="" className="form-inline justify-content-center"> 
            <div className="form-group">
              <input 
                className="form-control mr-2" 
                id="input" 
                type="text" 
                value={userInput} 
                onChange={handleInputChange}
              /> 
            </div>
            <div className="form-group">
              <button onClick={handleSubmit} className="btn btn-outline-dark" type="button">Speak</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default App;