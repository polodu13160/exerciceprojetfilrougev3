import { render, screen } from "@testing-library/react";
import App from "../src/App";

describe("UI test 3", () => {
  it("should display a button to submit typed text to the teenager", () => {
    // 1. Render App
    render(<App />);

    // 2. Expect that the button is there
    expect(screen.getByRole("button")).toBeInTheDocument();

    // 3. Expect that the button text is : "Submit"
    expect(screen.getByRole("button")).toHaveTextContent("Speak");
  });
});