import { render, fireEvent, screen } from '@testing-library/react';
import App from "../src/App";

test('As a user I want to receive "Calmez-vous, je sais ce que je fais !" when I type an uppercase question', () => {
  render(<App />);

  const input = screen.getByRole('textbox');
  fireEvent.change(input, { target: { value: 'ES TU STUPIDE ?' } });

  const button = screen.getByRole('button');
  fireEvent.click(button);

  expect(screen.getByText('Calmez-vous, je sais ce que je fais !')).toBeInTheDocument();
});

